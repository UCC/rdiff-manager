#! /usr/bin/python3

# rdiff-manager - a tool to run more than one rdiff-backup process concurrently
# and report their output in one hit

# Licensed under the MIT License:
# Copyright 2011-2012 David Adam <zanchey@ucc.gu.uwa.edu.au>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

configs_path = '/backups/conf'
backups_base = '/backups'
# number of jobs to run concurrently (0 for all)
concurrent_num = 0
duration_to_keep = '4W'

from multiprocessing import Pool
import time
from time import strftime
import subprocess, os, glob

run_rdiff = lambda *args: subprocess.check_output(('/usr/bin/rdiff-backup',) + args, stderr=subprocess.STDOUT).decode('utf-8')

class Host(object):
    def __init__(self, config):
        self.hostname = config['hostname']
        self.duration_to_keep =  config['duration_to_keep']
        self.run_backup_flags = config ['backup_flags']
        self.destination = config['backup_destination']
        self.include_file = config['include_file']

    def test_server(self):
        return run_rdiff('--test-server', 'root@%s::/' % self.hostname)

    def test_directory(self):
        # ensure destination is rdiff-backup target
        try:
            run_rdiff('--list-increments', self.destination)
        except subprocess.CalledProcessError:
            # check if base directory exists
            try:
                os.makedirs(self.destination)
            except OSError as e:
                if os.access(self.destination, os.X_OK):
                    pass
                else:
                    raise e
            # create baseline empty data directory
            run_rdiff(*(self.run_backup_flags.split(' ') + [ '--exclude',
                '**', 'root@%s::/' % (self.hostname), self.destination ] ) )
        else:
            pass


    def remove_old(self):
        # even with force, only increments are removed, never the most recent backup
        return run_rdiff('--remove-older-than', self.duration_to_keep, '--force', self.destination)

    def run_backup(self):
        return run_rdiff(*(self.run_backup_flags.split(' ') + [ '--include-globbing-filelist',
            self.include_file, 'root@%s::/' % (self.hostname), self.destination ] ) )

    def run_all(self):
        output = ''
        success = False
        try:
            # only add output if error occurs
            self.test_server()
            self.test_directory()
            # always print output
            output += self.remove_old()
            output += self.run_backup()
            success = True
        except subprocess.CalledProcessError as e:
            output += e.output.decode('utf-8')
            output += 'Backup failed with error %d' % e.returncode
        except OSError as e:
            output += 'Backup failed: %s (%d)' % (e.strerror, e.errno)
        return {'success': success, 'output': output}

def rdiff_backup(config):
    target = Host(config)
    return target.run_all()

if __name__ == '__main__':

    starttime = time.localtime()

    config_files = glob.iglob(configs_path + '/*.conf')
    filenames = (os.path.basename(x) for x in config_files)
    hostnames = (x.replace('.conf', '') for x in filenames)

    hosts = []

    for host in hostnames:
        hosts.append( { 'hostname': host,
            'include_file': '%s/%s.conf' % (configs_path, host),
            'duration_to_keep': duration_to_keep,
            'backup_flags': '--print-statistics --ssh-no-compression --exclude-sockets --exclude-device-files --exclude-fifos',
            'backup_destination': '%s/%s' % (backups_base, host),
            } )

    if concurrent_num == 0:
        concurrent_num = len(hosts)
    pool = Pool(concurrent_num)

    # Start the work and wait for it to finish
    results = { host['hostname']: pool.apply_async(rdiff_backup, (host, )) for host in hosts }
    pool.close()
    pool.join()

    divider = '-' * 76
    # Header
    print(divider)
    print('RDIFF-MANAGER REPORT for run started', strftime('%c', starttime))
    print(divider)

    # Summary
    print()
    print('SUMMARY:')
    print()
    for k, v in results.items():
        if v.get()['success']:
            print('  Backup succeeded for', k)
        else:
            print('  Backup FAILED for', k)
    print()

    for k, v in results.items():
        print(divider)
        print("Backup results for", k)
        print()
        print(v.get()['output'])
        print()
